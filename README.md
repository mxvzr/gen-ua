Generate an user agent string using package.json files found in parent directories

# Usage

var UA = require('gen-ua')(__dirname); // defaults to process.cwd();
> "parent/1.0.0 dependency/1.0.0 subdependency/1.0.0"

# Info

File reads are synchronous
Broken package.json files are silently ignored