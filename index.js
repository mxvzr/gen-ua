const FPJ = require('find-package-json');

module.exports = function(dir) {
	var finder = FPJ(dir), stack = [], last;
	do {
		last = finder.next();
		last.value && stack.unshift(last.value.name + '/' + last.value.version);
	} while (!last.done);

	return stack.join(' ');
};