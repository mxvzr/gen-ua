var genua = require('./'),
	util = require('util'),
	assert = require('assert'),
	fs = require('fs');

const PATH1 = __dirname;
const PATH2 = PATH1 + '/fixture';
const PATH3 = PATH2 + '/broken';

const UA1_CTRL = 'gen-ua/' + require('find-package-json')().next().value.version;
const UA2_CTRL = UA1_CTRL + ' fixture/1.0.0';
const UA3_CTRL = UA2_CTRL;

const MSG_PRE = 'genua(%s) == "%s"';
const MSG_POST = '> "%s"';

function test(PATH, CTRL) {
	console.log(util.format(MSG_PRE, PATH, CTRL));
	var UA = genua(PATH);
	console.log(util.format(MSG_POST, UA));
	assert.equal(UA, CTRL); //, 'Invalid user agent');
}

test(PATH1, UA1_CTRL);
test(PATH2, UA2_CTRL);
test(PATH3, UA3_CTRL);